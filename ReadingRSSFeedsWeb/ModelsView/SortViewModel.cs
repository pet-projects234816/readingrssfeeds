﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadingRSSFeedsWeb.ModelsView
{
    public class SortViewModel
    {
        public SortState FeedSort { get; set; } // значение для сортировки по источнику новости
        public SortState PublishDateSort { get; set; }    // значение для сортировки по дате публикации
        public SortState Current { get; set; }     // значение свойства, выбранного для сортировки
        public bool Up { get; set; }  // Сортировка по возрастанию или убыванию

        public SortViewModel(SortState sortOrder)
        {
            // значения по умолчанию
            FeedSort = SortState.FeedAsc;
            PublishDateSort = SortState.PublishDateAsc;

            Up = true;

            if (sortOrder == SortState.FeedDesc || sortOrder == SortState.PublishDateDesc)
            {
                Up = false;
            }

            switch (sortOrder)
            {
                case SortState.FeedDesc:
                    Current = FeedSort = SortState.FeedAsc;
                    break;
                case SortState.PublishDateAsc:
                    Current = PublishDateSort = SortState.PublishDateDesc;
                    break;
                case SortState.PublishDateDesc:
                    Current = PublishDateSort = SortState.PublishDateAsc;
                    break;
                default:
                    Current = FeedSort = SortState.FeedDesc;
                    break;
            }
        }
    }
}


public enum SortState
{
    FeedAsc,
    FeedDesc,
    PublishDateAsc,
    PublishDateDesc
}


