﻿using CommonLib;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadingRSSFeedsWeb.ModelsView
{
    public class FilterViewModel
    {
        public SelectList Feeds { get; private set; } // список RSS лент
        public int? SelectedFeed { get; private set; }   // выбранная RSS лента

        public FilterViewModel(List<Feed> feeds, int? feed)
        {
            // устанавливаем начальный элемент, который позволит выбрать всех
            feeds.Insert(0, new Feed { Host = "Все", Id = 0 });
            Feeds = new SelectList(feeds, "Id", "Host", feed);
            SelectedFeed = feed;
        }

    }
}
