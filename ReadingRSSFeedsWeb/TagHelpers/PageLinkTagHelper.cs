﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using ReadingRSSFeedsWeb.ModelsView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadingRSSFeedsWeb.TagHelpers
{
    public class PageLinkTagHelper : TagHelper
    {
        private IUrlHelperFactory urlHelperFactory;

        public PageLinkTagHelper(IUrlHelperFactory helperFactory)
        {
            urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }
        public PageViewModel PageModel { get; set; }
        public string PageAction { get; set; }

        [HtmlAttributeName(DictionaryAttributePrefix = "page-url-")]
        public Dictionary<string, object> PageUrlValues { get; set; } = new Dictionary<string, object>();

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            IUrlHelper urlHelper = urlHelperFactory.GetUrlHelper(ViewContext);
            output.TagName = "nav";
            output.Attributes.SetAttribute("aria-label", "...");

            // набор ссылок будет представлять список ul
            TagBuilder tag = new TagBuilder("ul");
            tag.AddCssClass("pagination");
            tag.AddCssClass("pagination-lg");


            // на текущую, предыдущую и следующую + Previos + Next + FastTo1 + FastToLast

            // Быстрый переход на первую страницу
            if (PageModel.PageNumber > 2)
            {
                TagBuilder fastTo1 = CreateTag(1, urlHelper, IsPrevNext.None);
                tag.InnerHtml.AppendHtml(fastTo1);

                TagBuilder dots = CreateTag(-1, urlHelper, IsPrevNext.Dots);
                tag.InnerHtml.AppendHtml(dots);
            }

            // Previos
            TagBuilder previosBtn = CreateTag(PageModel.PageNumber - 1, urlHelper, IsPrevNext.Previos);
            tag.InnerHtml.AppendHtml(previosBtn);

            // 3-я страница с конца, если текущая: последняя страница
            if (!PageModel.HasNextPage && PageModel.TotalPages > 2)
            {
                TagBuilder item3 = CreateTag(PageModel.TotalPages - 2, urlHelper, IsPrevNext.None);
                tag.InnerHtml.AppendHtml(item3);
            }

            // создаем ссылку на предыдущую страницу, если она есть
            if (PageModel.HasPreviousPage)
            {
                TagBuilder prevItem = CreateTag(PageModel.PageNumber - 1, urlHelper, IsPrevNext.None);
                tag.InnerHtml.AppendHtml(prevItem);
            }

            // Текущая страница
            TagBuilder currentItem = CreateTag(PageModel.PageNumber, urlHelper, IsPrevNext.None);
            tag.InnerHtml.AppendHtml(currentItem);


            // создаем ссылку на следующую страницу, если она есть
            if (PageModel.HasNextPage)
            {
                TagBuilder nextItem = CreateTag(PageModel.PageNumber + 1, urlHelper, IsPrevNext.None);
                tag.InnerHtml.AppendHtml(nextItem);
            }

            // 3-я страница если текущая: 1
            if (!PageModel.HasPreviousPage && PageModel.TotalPages > 2)
            {
                TagBuilder item3 = CreateTag(PageModel.PageNumber + 2, urlHelper, IsPrevNext.None);
                tag.InnerHtml.AppendHtml(item3);
            }


            // Next
            TagBuilder nextBtn = CreateTag(PageModel.PageNumber + 1, urlHelper, IsPrevNext.Next);
            tag.InnerHtml.AppendHtml(nextBtn);


            // Быстрый переход на последнюю страницу
            if (PageModel.PageNumber < PageModel.TotalPages - 1)
            {
                TagBuilder dots = CreateTag(-1, urlHelper, IsPrevNext.Dots);
                tag.InnerHtml.AppendHtml(dots);

                TagBuilder fastToLast = CreateTag(PageModel.TotalPages, urlHelper, IsPrevNext.None);
                tag.InnerHtml.AppendHtml(fastToLast);

            }

            output.Content.AppendHtml(tag);
        }

        TagBuilder CreateTag(int pageNumber, IUrlHelper urlHelper, IsPrevNext isPrevNext)
        {
            TagBuilder item = new TagBuilder("li");
            TagBuilder link = new TagBuilder("a");

            item.AddCssClass("page-item");
            link.AddCssClass("page-link");
            link.AddCssClass("partial");

            if (isPrevNext != IsPrevNext.None)
            {
                if (pageNumber < 1 || pageNumber > PageModel.TotalPages || isPrevNext == IsPrevNext.Dots)
                {
                    item.AddCssClass("disabled");

                    var style = "border-color: whitesmoke; color: lightgrey; ";
                    if (isPrevNext == IsPrevNext.Dots)
                        style += "border: 0px; margin-left: 1px; margin-right: 1px; ";
                    link.Attributes.Add("style", style);
                }

                // Для Previos и Next
                if (isPrevNext != IsPrevNext.Dots)
                {
                    link.Attributes.Add("aria-label", $"{isPrevNext.ToString()}");

                    TagBuilder span1 = new TagBuilder("span");
                    span1.Attributes.Add("aria-hidden", "true");
                    if (isPrevNext == IsPrevNext.Previos)
                        span1.InnerHtml.AppendHtml("&laquo;");
                    if (isPrevNext == IsPrevNext.Next)
                        span1.InnerHtml.AppendHtml("&raquo;");
                    link.InnerHtml.AppendHtml(span1);

                    TagBuilder span2 = new TagBuilder("span");
                    span2.AddCssClass("sr-only");
                    span2.InnerHtml.Append($"{isPrevNext.ToString()}");
                    link.InnerHtml.AppendHtml(span2);
                }
                else // Для Dots
                {
                    link.InnerHtml.AppendHtml("&hellip;");
                }
            }
            else
            {
                link.InnerHtml.Append(pageNumber.ToString());
            }

            if (pageNumber == this.PageModel.PageNumber)
            {
                item.AddCssClass("active");
            }
            else
            {
                if (pageNumber > 0)
                {
                    PageUrlValues["page"] = pageNumber;
                    PageUrlValues["partial"] = true;
                    link.Attributes["href"] = urlHelper.Action(PageAction, PageUrlValues);
                }
            }

            item.InnerHtml.AppendHtml(link);
            return item;
        }

        public enum IsPrevNext
        {
            Previos,
            Next,
            Dots,
            None
        }
    }
}
