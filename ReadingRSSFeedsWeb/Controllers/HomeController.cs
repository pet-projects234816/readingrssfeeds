﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CommonLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ReadingRSSFeedsWeb.Models;
using ReadingRSSFeedsWeb.ModelsView;

namespace ReadingRSSFeedsWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly RSSReadingDbContext _db;


        public HomeController(RSSReadingDbContext сontext)
        {
            _db = сontext;
        }

        public async Task<IActionResult> Index(int? feed, int page = 1,
            SortState sortOrder = SortState.PublishDateDesc, bool partial = false)
        {
            int pageSize = 10;

            //фильтрация
            List<Feed> eer = _db.Feeds.ToList();

            IQueryable<FeedItem> feedItems =
                _db.FeedItems.Include(x => x.Feed)
                   .Where(p => p.FeedId == feed
                                || feed == null
                                || feed == 0);

            // сортировка
            switch (sortOrder)
            {
                case SortState.FeedDesc:
                    feedItems = feedItems.OrderByDescending(s => s.Feed.Host);
                    break;
                case SortState.FeedAsc:
                    feedItems = feedItems.OrderBy(s => s.Feed.Host);
                    break;
                case SortState.PublishDateAsc:
                    feedItems = feedItems.OrderBy(s => s.PublishingDate);
                    break;
                default:
                    feedItems = feedItems.OrderByDescending(s => s.PublishingDate);
                    break;
            }

            // пагинация
            var count = await feedItems.CountAsync();
            var items = await feedItems.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            // модель представления
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = new PageViewModel(count, page, pageSize),
                SortViewModel = new SortViewModel(sortOrder),
                FilterViewModel = new FilterViewModel(_db.Feeds.ToList(), feed),
                FeedItems = items
            };


            if (partial)
                return View("_PartialTablePagination", viewModel);
            else
                return View(viewModel);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
