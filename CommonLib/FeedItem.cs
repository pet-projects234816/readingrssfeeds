﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CommonLib
{
    /// <summary>
    /// Новость из RSS ленты
    /// </summary>
    public class FeedItem
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Id RSS новости
        /// </summary>
        public string RSSItemId { get; set; }

        /// <summary>
        /// Название новости
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Источник новость
        /// </summary>
        public string Link { get; set; }


        /// <summary>
        /// Описание новости
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Дата публикации
        /// </summary>
        public DateTime? PublishingDate { get; set; }


        // Навигационные свойства
        public int FeedId { get; set; } // ссылка на связанную модель Feed
        public Feed Feed { get; set; }


    }
}