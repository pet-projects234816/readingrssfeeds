﻿using Microsoft.EntityFrameworkCore;
using System;

namespace CommonLib
{
    public class RSSReadingDbContext : DbContext
    {
        public DbSet<Feed> Feeds { get; set; }
        public DbSet<FeedItem> FeedItems { get; set; }

        public RSSReadingDbContext(DbContextOptions<RSSReadingDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
